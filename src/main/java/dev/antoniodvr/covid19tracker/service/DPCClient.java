package dev.antoniodvr.covid19tracker.service;

import dev.antoniodvr.covid19tracker.model.CountryData;
import dev.antoniodvr.covid19tracker.model.ProvinceData;
import dev.antoniodvr.covid19tracker.model.RegionData;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.stream.Stream;

@Repository
public class DPCClient {

    private static final String COUNTRY_DATA_SOURCE_ENDPOINT = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-andamento-nazionale.json";
    private static final String REGION_DATA_SOURCE_ENDPOINT = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-regioni.json";
    private static final String PROVINCE_DATA_SOURCE_ENDPOINT = "https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-json/dpc-covid19-ita-province.json";

    private RestTemplate restTemplate;

    public DPCClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Stream<CountryData> getCountryData() {
        ResponseEntity<CountryData[]> countryDataResponse = restTemplate.getForEntity(COUNTRY_DATA_SOURCE_ENDPOINT, CountryData[].class);

        if(countryDataResponse.getBody() == null) {
            return Stream.empty();
        }

        return Arrays.stream(countryDataResponse.getBody());
    }

    public Stream<RegionData> getRegionData() {
        ResponseEntity<RegionData[]> regionDataResponse = restTemplate.getForEntity(REGION_DATA_SOURCE_ENDPOINT, RegionData[].class);

        if(regionDataResponse.getBody() == null) {
            return Stream.empty();
        }

        return Arrays.stream(regionDataResponse.getBody());
    }

    public Stream<ProvinceData> getProvinceData() {
        ResponseEntity<ProvinceData[]> provinceDataResponse = restTemplate.getForEntity(PROVINCE_DATA_SOURCE_ENDPOINT, ProvinceData[].class);

        if(provinceDataResponse.getBody() == null) {
            return Stream.empty();
        }

        return Arrays.stream(provinceDataResponse.getBody());
    }

}
